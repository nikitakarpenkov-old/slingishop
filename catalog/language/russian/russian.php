<?php
// OXY Theme
$_['text_sale']                  = 'Распродажа';
$_['text_more']                  = 'Подробнее';
$_['text_share']                 = 'Поделиться';
$_['text_shop_now']              = 'Купить!';
$_['text_next_product']          = 'Назад';
$_['text_previous_product']      = 'Назад';
$_['text_percent_saved']         = 'Экономия:';
$_['text_product_viewed']        = 'Продукт просматривали:';
$_['text_product_friend']        = 'Отправить другу';
$_['text_menu_categories']       = 'Категории';
$_['text_menu_brands']           = 'Бренды';
$_['text_menu_contact_us']       = 'Свяжитесь с нами';
$_['text_menu_contacts']         = 'Контакты';
$_['text_menu_contact_address']  = 'Адрес';
$_['text_menu_contact_hours']    = 'Часы работы';
$_['text_menu_contact_form']     = 'Форма связи';
$_['text_menu_contact_map']      = 'Наше местоположение';
$_['text_menu_menu']             = 'Меню';

// Locale
$_['code']                  = 'ru';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l d F Y';
$_['time_format']           = 'H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = '';

// Text
$_['text_home']             = 'Главная';
$_['text_yes']              = 'Да';
$_['text_no']               = 'Нет';
$_['text_none']             = ' --- Не выбрано --- ';
$_['text_select']           = ' --- Выберите --- ';
$_['text_all_zones']        = 'Все зоны';
$_['text_pagination']       = 'Показано с {start} по {end} из {total} (всего {pages} страниц)';
$_['text_separator']        = ' &raquo; ';

// Buttons
$_['button_add_address']    = 'Добавить адрес';
$_['button_back']           = 'Назад';
$_['button_continue']       = 'Продолжить';
$_['button_cart']           = 'Купить';
$_['button_compare']        = 'В сравнение';
$_['button_wishlist']       = 'В закладки';
$_['button_checkout']       = 'Оформление заказа';
$_['button_confirm']        = 'Подтверждение заказа';
$_['button_coupon']         = 'Применение купона';
$_['button_delete']         = 'Удалить';
$_['button_download']       = 'Скачать';
$_['button_edit']           = 'Редактировать';
$_['button_filter']         = 'Уточнение поиска';
$_['button_new_address']    = 'Новый адрес';
$_['button_change_address'] = 'Изменить адрес';
$_['button_reviews']        = 'Отзывы';
$_['button_write']          = 'Написать отзыв';
$_['button_login']          = 'Войти';
$_['button_update']         = 'Обновить';
$_['button_remove']         = 'Удалить';
$_['button_reorder']        = 'Дополнительный заказ';
$_['button_return']         = 'Возврат товара';
$_['button_shopping']       = 'Продолжить покупки';
$_['button_search']         = 'Поиск';
$_['button_shipping']       = 'Применить Доставку';
$_['button_guest']          = 'Оформление заказа без регистрации';
$_['button_view']           = 'Просмотр';
$_['button_voucher']        = 'Применить подарочный сертификат';
$_['button_upload']         = 'Загрузить файл';
$_['button_reward']         = 'Применить бонусные баллы';
$_['button_quote']          = 'Узнать цены';

// Error
$_['error_upload_1']        = 'Загружаемый на сервер файл превышает параметр upload_max_filesize в php.ini!';
$_['error_upload_2']        = 'Загружаемый на сервер файл превышает параметр MAX_FILE_SIZE который определен в HTML форме!';
$_['error_upload_3']        = 'Загружаемый на сервер файл был загружен не полностью!';
$_['error_upload_4']        = 'Файл не был загружен!';
$_['error_upload_6']        = 'Отсутствует временная папка!';
$_['error_upload_7']        = 'Не удалось записать файл на диск!';
$_['error_upload_8']        = 'Загружаемый на сервер файл не подходит по расширению!';
$_['error_upload_999']      = 'Неизвестная ошибка!';
?>